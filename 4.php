<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 4</title>
</head>

<body>
    <form action="ejercicio4Destino.php">
        <div>
            <label for="nombre">Nombre:</label>
            <input type="text" name="nombre" id="nombre" placeholder="Introduce tu nombre">
        </div>
        <div>
            <label for="email">Email:</label>
            <input type="email" name="email" id="email" placeholder="Introduce tu email">
        </div>
        <div>
            <label for="telefono">Telefono:</label>
            <input type="text" name="telefono" id="telefono" placeholder="Introduce tu telefono">
        </div>
        <div>
            <button formmethod="get">Enviar por GET</button>
            <button formmethod="post">Enviar por POST</button>
        </div>
    </form>
</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 2 destino</title>
</head>

<body>

    <!-- Metodo largo -->
    Hola <?php echo $_POST['nombre']; ?><br>
    Tu email es <?php echo $_POST['email']; ?>

    <br>

    <!-- Metodo corto -->
    Hola <?= $_POST['nombre'] ?><br>
    Tu email es <?= $_POST['email'] ?>
</body>

</html>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 5</title>
</head>

<body>
    <form action="ejercicio5Destino.php">
        <div>
            <label for="nombre">Nombre </label>
            <input type="text" name="nombre" id="nombre" placeholder="Introduce tu nombre" required>
        </div>
        <br>
        <div>
            <label for="apellidos">Apellidos </label>
            <input type="text" name="apellidos" id="apellidos" placeholder="Introduce tus apellidos" required>
        </div>
        <br>
        <div>
            <label for="cpostal">Codigo Postal </label>
            <input type="text" name="cpostal" id="cpostal" placeholder="Introduce tu codigo postal" required>
        </div>
        <br>
        <div>
            <label for="telefono">Telefono </label>
            <input type="text" name="telefono" id="telefono" placeholder="Introduce tu telefono" required>
        </div>
        <br>
        <div>
            <label for="correo">Correo </label>
            <input type="email" name="correo" id="correo" placeholder="Introduce tu correo" required>
        </div>
        <br>
        <div>
            <button>Enviar</button>
        </div>
    </form>
</body>

</html>
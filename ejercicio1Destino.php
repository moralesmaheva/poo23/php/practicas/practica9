<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Ejercicio 1 destino</title>
</head>

<body>

    <!-- Metodo largo -->
    Hola <?php echo $_GET['nombre']; ?><br>
    Tu email es <?php echo $_GET['email']; ?>

    <br>

    <!-- Metodo corto -->
    Hola <?= $_GET['nombre']; ?><br>
    Tu email es <?= $_GET['email']; ?>
</body>

</html>